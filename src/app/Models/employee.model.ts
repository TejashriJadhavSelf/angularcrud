export class Employee
{
    id:number;
    fName:string;
    gender:string;
    email?:string;
    phoneNumber?:number;
    contactPreference:string;
    dateOfBirth:Date;
    department:string;
    isActive:boolean;
    photoPath?:string;
}