import { Component, OnInit } from '@angular/core';
import { Employee } from '../Models/employee.model';
import { EmployeeService } from './employee.service';
@Component({
  templateUrl: './list-employees.component.html',
  styleUrls: ['./list-employees.component.css']
})
export class ListEmployeesComponent implements OnInit {
public employees: Employee[]=[];
filteredEmployees: Employee[];



  constructor(private _employeeService:EmployeeService) { }

  ngOnInit(): void {
     this.employees= this._employeeService.getEmployees();
  }

}
