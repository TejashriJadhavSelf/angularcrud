import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Employee } from '../models/employee.model';
import { Department } from '../models/department.model';
import { EmployeeService } from './employee.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  public employee: Employee = {
    id : null,
    fName : null,
    gender : null,
    email : null,
    phoneNumber : null,
    contactPreference : null,
    dateOfBirth : null,
    department : null,
    isActive : null,
    photoPath : null
  };

 public departments: Department[] = [
    {id: 1, Name: 'Help Desk'},
    {id: 2, Name: 'HR'},
    {id: 3, Name: 'IT'},
    {id: 4, Name: 'Payroll'}
  ];

  model: any = {};
  
  constructor(private _employeeService: EmployeeService,
              private _router: Router) { }
  
  ngOnInit(): void {
  }

saveEmployee(): void{
  this._employeeService.save(this.employee);
  this._router.navigate(['list']);
}

}
